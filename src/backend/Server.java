package backend;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

//WHY IT GOTTA BE LIKE THAT

public class Server extends Thread{

	//max 100 customers
	public int ID;
	public BlockingQueue<Customer> queue = new ArrayBlockingQueue<Customer>(100);
		private int minServiceTime, maxServiceTime, minArrivingTime, maxArrivingTime;
	private volatile boolean endSim = false;
	private Store store;
	
	Server(Store store, int ID, int minST, int maxST, int minAT, int maxAT)
	{
		this.ID = ID;
		this.minServiceTime = minST;
		this.maxServiceTime = maxST;
		this.minArrivingTime = minAT;
		this.maxArrivingTime = maxAT;
		this.store = store;
	}
	
	
	public Server() {
		this.ID = 0;
		this.minServiceTime = 0;
		this.maxServiceTime = 0;
		this.minArrivingTime = 0;
		this.maxArrivingTime = 0;
	}


	@Override
	public synchronized void run() {
		while(!endSim) {
			try {
				while(this.queue.size()==0)
				{
					wait();
				}
				
				if(this.store.simTime > this.queue.peek().getArrivalTime()+this.queue.peek().getProcessTime()) {
				this.customerLeave(this.queue.peek().getProcessTime());
				}
				else 
				{
					wait();
				}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public synchronized  void addCustomer(Customer c, int currentTime)
	{
		c.setWaitTime(this.calculateWaitTime());
		c.setArrivalTime(currentTime);
		
		this.queue.add(c);
		String log = "Customer" + " has arrived at queue no." + this.ID + "| avg processing time: " + avgProcTime();
		
		System.out.println(log);
		notifyAll();
	}
	
	public synchronized void customerLeave(int currentTime) throws InterruptedException {
		while(this.queue.size() == 0)
		{
			wait();
		}
		
		float avgTime = calculateWaitTime()/this.queue.size();
		
		String log = "Customer" + " has left queue no." + this.ID + "| avg Waiting Time:" + avgTime;
		this.queue.poll();
		System.out.println(log);
		notifyAll();
	}
	
	public synchronized String print()
	{
		String queue = "";
		int cachedQueueSize = this.queue.size(); //minor efficiency improvement by caching size of queue
		
		for(int i = 0; i < cachedQueueSize; i++)
		{
			queue+="\u263A ";
		}
		
		return queue;
	}
	
	
	private synchronized int avgProcTime()
	{
		int procTime = 0;
		for(Customer i : this.queue)
		{
			procTime += i.getProcessTime();
		}
		
		procTime/=this.queue.size();
		return procTime;
	}
	
	@SuppressWarnings("unused")
	public synchronized int calculateWaitTime()
	{
		int waitTime=0;
		
		for(Customer i:this.queue)
		{
			waitTime+= i.getProcessTime();
		}
		
		return waitTime;
	}
	
	public void setEndSim(boolean value)
	{
		this.endSim = value;
	}
	
}
