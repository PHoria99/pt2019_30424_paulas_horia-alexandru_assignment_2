package backend;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

import graphical.FirstView;

public class Store extends Thread{

	private volatile List<Server> queues = new ArrayList<Server>();
	public volatile int simTime = 0;
	private volatile boolean endSim = false;
	private int maxAT, minAT, maxPT, minPT;
	private int maxSimTime =100;
	private FirstView queuesShow;
	
	
	public Store(int queueNR, int minArrTime, int maxArrTime, int minProcTime, int maxProcTime, int maxSimTime, FirstView stuff){
		this.maxAT = maxArrTime;
		this.minAT = minArrTime;
		this.maxPT = maxProcTime;
		this.minPT = minProcTime;
		this.maxSimTime = maxSimTime;
		this.queuesShow = stuff;
		
		for(int i = 0; i < queueNR; i++)
		{
			queues.add(new Server(this, i+1, this.minPT, this.maxPT, this.minAT, this.maxAT));
		}
	}
	
	public Store() {
		this.maxAT = 0;
		this.minAT = 0;
		this.maxPT = 0;
		this.minPT = 0;
		this.queuesShow = null;
	}

	public synchronized Server getSmallestQueue()
	{
		Server lady = new Server();
		int minSize = 99999999;
		
		for(Server i : this.queues)
		{
			if(i.queue.size() < minSize) {minSize = i.queue.size(); lady = i;}
		}
		
		notifyAll();
		return lady;
	}
	
	public synchronized void run()
	{
		int id=1;
		int clients = 0;
		
		for(Server i : this.queues)
		{
			i.start();
		}
		
		while(!endSim)
		{
			double nextArrTime = Math.random()*(this.maxAT - this.minAT) + this.minAT;

			int arrTimeOfLastCustomer = simTime;
			simTime++;
			String ceva = new String();
			
			for(Server i : this.queues)
			{
				ceva+= i.print() + "\n";
			}
			
			this.queuesShow.queues.setText(ceva);
			
			Customer c = new Customer(id, this.minPT, this.maxPT, simTime);
			try {
				if(clients == 0)
				{
					Thread.sleep((int)nextArrTime*1000);
					arrTimeOfLastCustomer = c.getArrivalTime();
					simTime += (int)nextArrTime;
					clients++;
				}
				else
				{
					Thread.sleep((int)nextArrTime*1000);
					arrTimeOfLastCustomer = c.getArrivalTime() - arrTimeOfLastCustomer;
					simTime +=(int) nextArrTime;
					clients++;
				}
				this.getSmallestQueue().addCustomer(c, simTime);
			}catch(Exception e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(1000);
			}catch(Exception e) {
				e.printStackTrace();
			}
			id++;
			
			if(simTime==this.maxSimTime) endSim = true;
		}
		
		if(endSim) {
		for(Server i : this.queues)
		{
			i.setEndSim(true);
		}}
	}
	
	public synchronized  void setEndSim(boolean value)
	{
		this.endSim = true;
	}
}
