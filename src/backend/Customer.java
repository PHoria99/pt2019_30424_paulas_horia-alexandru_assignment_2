package backend;

public class Customer {
	private int processTime;	//Waiting time is calculated, processing time is random for each customer
	public int ID;
	private int arrivalTime;
	private int waitTime;
	
	Customer()
	{
		this.processTime = 1;
	}
	
	Customer(int ID, int minProcessTime, int maxProcessTime, int time){
		this.processTime = minProcessTime + (int) (Math.random()*(maxProcessTime - minProcessTime));
		this.ID = ID;
		this.arrivalTime = time;
	}
	
	public synchronized int getProcessTime() {
		return this.processTime;
	}

	public synchronized int getArrivalTime() {
		return arrivalTime;
	}

	public synchronized void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public synchronized int getWaitTime() {
		return waitTime;
	}

	public synchronized void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	
}
