package graphical;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextArea;

import backend.Store;

public class Controller implements ActionListener{
	//lazy
	private FirstView view1;
	private Store store;
	
	public Controller(FirstView view1, Store store)
	{
		this.view1 = view1;
		this.store = store;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		
		if( b == this.view1.startSim)
		{
			this.store = new Store(Integer.parseInt(this.view1.queueNo.getText()),
					Integer.parseInt(this.view1.minAT.getText()),
					Integer.parseInt(this.view1.maxAT.getText()),
					Integer.parseInt(this.view1.minPT.getText()),
					Integer.parseInt(this.view1.maxPT.getText()),
					Integer.parseInt(this.view1.simEnd.getText()),
					this.view1);
			
			this.store.start();
		}
	}

}
