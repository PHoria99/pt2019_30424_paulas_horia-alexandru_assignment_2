package graphical;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MainView {
	
	public JFrame frame = new JFrame();
	private JPanel window;
	private JScrollPane scroallableArea;
	private JTextArea log, queues, avgAT, avgWT, avgET, peakHour;
	private JLabel logL, queuesL, avgATL, avgWTL, avgETL, peakHourL;
	
	MainView()
	{
		this.initialize();
	}
	
	private void initialize() 
	{
		this.frame.setSize(new Dimension(800, 600));
		this.window = new JPanel(new GridBagLayout());
		this.frame.add(window);
		
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1.0f; c.weighty = 1.0f;
		c.gridx = 1; c.gridy = 1;
		
		this.queues = new JTextArea();
		this.scroallableArea = new JScrollPane(this.queues, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.frame.add(this.scroallableArea);
	}
	
}
