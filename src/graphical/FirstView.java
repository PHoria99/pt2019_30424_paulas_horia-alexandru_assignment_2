package graphical;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class FirstView {
	public JFrame frame = new JFrame();
	
	private JPanel window;
	private JLabel WTL,queueNoL, PTL, simEndL, ATL, minL, maxL;
	public JTextArea minWT, maxWT, queueNo, minPT, maxPT, simEnd, minAT, maxAT;
	public JButton startSim;
	private JScrollPane scroallableArea;
	public JTextArea queues;
	
	FirstView()
	{
		initialize();
	}
	
	private void initialize()
	{
		this.window = new JPanel();
		this.window.setLayout(new GridBagLayout());
		this.frame.setSize(new Dimension(640, 480));
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.frame.add(window);
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;

		c.weightx = 20;
		c.weighty = 10.f;

		c.gridy=0; c.gridx=2;
		this.minL = new JLabel("Min:");
		this.window.add(minL, c);

		c.gridy=0; c.gridx=4;
		this.maxL = new JLabel("Max:");
		this.window.add(maxL, c);
		
		
		c.gridy = 1; c.gridx = 1;
		this.WTL = new JLabel();
		this.WTL.setText("Waiting Time Interval:");
		this.window.add(WTL,c);
		
		c.gridy = 2; c.gridx = 1;
		this.PTL = new JLabel();
		this.PTL.setText("Processing Time Interval:");
		this.window.add(PTL,c);
		
		c.gridy = 3; c.gridx = 1;
		this.ATL = new JLabel();
		this.ATL.setText("Arrival Time Interval");
		this.window.add(ATL,c);
		
		c.gridy = 4; c.gridx = 1;
		this.queueNoL = new JLabel();
		this.queueNoL.setText("Number of queues:");
		this.window.add(queueNoL,c);
		
		c.gridy = 5; c.gridx = 1;
		this.simEndL = new JLabel();
		this.simEndL.setText("End Simulation at:");
		this.window.add(simEndL,c);
		
		c.gridy = 1; c.gridx = 2;
		this.minWT=new JTextArea();
		this.minWT.setText("8");
		this.window.add(this.minWT,c);
		
		c.gridy = 1; c.gridx = 4;
		this.maxWT=new JTextArea();
		this.maxWT.setText("18");
		this.window.add(this.maxWT,c);

		c.gridy = 2; c.gridx = 2;
		this.minPT=new JTextArea();
		this.minPT.setText("5");
		this.window.add(this.minPT,c);
		
		c.gridy = 2; c.gridx = 4;
		this.maxPT=new JTextArea();
		this.maxPT.setText("15");
		this.window.add(this.maxPT,c);

		c.gridy = 3; c.gridx = 2;
		this.minAT=new JTextArea();
		this.minAT.setText("2");
		this.window.add(this.minAT,c);
		
		c.gridy = 3; c.gridx = 4;
		this.maxAT=new JTextArea();
		this.maxAT.setText("6");
		this.window.add(this.maxAT,c);
		
		c.gridy = 4; c.gridx = 2;
		this.queueNo = new JTextArea();
		this.queueNo.setText("3");
		this.window.add(this.queueNo,c);

		c.gridy = 5; c.gridx = 2;
		this.simEnd = new JTextArea();
		this.simEnd.setText("75");
		this.window.add(this.simEnd,c);
		
		c.gridy = 7; c.gridx = 3;
		this.startSim = new JButton("Start Simulation");
		this.window.add(startSim,c);
		
		c.gridx=1; c.gridy = 10;
		c.fill = GridBagConstraints.BOTH;
		this.queues = new JTextArea("");
		this.scroallableArea = new JScrollPane(this.queues);
		this.scroallableArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.window.add(this.scroallableArea,c);
	}
	
}
