package graphical;

import backend.Store;

public class Main {
	public static void main(String[] args)
	{
		try {
			FirstView start = new FirstView();
			Store store = new Store();
			Controller control = new Controller(start, store);
			
			start.frame.setVisible(true);
			//window.frame.setVisible(true);
			
			start.startSim.addActionListener(control);
			
			}
			catch( Exception e) {
				e.printStackTrace();
			}
	}
}
